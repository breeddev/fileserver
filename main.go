package main

import (
        "flag"
        "fmt"
        "html/template"
        "io"
        "io/ioutil"
        "log"
        "mime/multipart"
        "net/http"
        "os"
        "sync"
)

func usage() {
        fmt.Println("usage: fileserver [-p port]")
        flag.PrintDefaults()
        os.Exit(2)
}

func main() {
        flag.Usage = usage

        port := flag.Int("p", 8080, "port to listen on")
        flag.Parse()

        upload = &uploadData{}

        os.Mkdir("assets", os.ModeDir|os.ModePerm)

        http.HandleFunc("/", rootHandler)
        http.HandleFunc("/upload", uploadHandler)
        http.HandleFunc("/delete", deleteHandler)
        http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))

        log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))
}

func init() {
        // Load the template from the directory or from the internal variable
        defer func() {
                if r := recover(); r != nil {
                        template.Must(tpl.Parse(tmpl))
                }
        }()
        tpl = template.New("upload.html")
        tpl = template.Must(tpl.ParseFiles("tmpl/upload.html"))
}

var tpl *template.Template

func display(w http.ResponseWriter, name string, data interface{}) {
        tpl.ExecuteTemplate(w, name+".html", data)
}

func rootHandler(w http.ResponseWriter, req *http.Request) {
        switch req.Method {
        case "GET":
                upload.Message = "Choose a file to upload."
                upload.load()
                display(w, "upload", upload)
        default:
                w.WriteHeader(http.StatusMethodNotAllowed)
        }
}

func uploadHandler(w http.ResponseWriter, req *http.Request) {
        switch req.Method {
        case "POST":
                reader, err := req.MultipartReader()
                if err != nil {
                        http.Error(w, err.Error(), http.StatusInternalServerError)
                        return
                }

                for part, err := reader.NextPart(); err != io.EOF; part, err = reader.NextPart() {
                        if err != nil {
                                http.Error(w, err.Error(), http.StatusInternalServerError)
                        }
                        if part.FileName() == "" {
                                continue
                        }
                        createFile(part, w)
                }
                http.Redirect(w, req, req.Referer(), http.StatusSeeOther)
        default:
                w.WriteHeader(http.StatusMethodNotAllowed)
        }
}

func deleteHandler(w http.ResponseWriter, req *http.Request) {
        switch req.Method {
        case "POST":
                file := req.FormValue("file")
                if file == "" {
                        http.Error(w, "No file name provided", http.StatusInternalServerError)
                        return
                }

                remove(file)

                http.Redirect(w, req, req.Referer(), http.StatusSeeOther)
        default:
                w.WriteHeader(http.StatusMethodNotAllowed)
        }
}

func createFile(part *multipart.Part, w http.ResponseWriter) {
        upload.mu.Lock()
        defer upload.mu.Unlock()

        // create destination file making sure the path is writable.
        dst, err := os.Create("assets/" + part.FileName())
        defer dst.Close()
        if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
        }

        // copy the uploaded file to the destination file
        if _, err := io.Copy(dst, part); err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
        }
}

func remove(file string) error {
        upload.mu.Lock()
        defer upload.mu.Unlock()

        if err := os.Remove("assets/" + file); !os.IsNotExist(err) {
                return err
        }
        return nil
}

type uploadData struct {
        Message string
        Files   []string
        mu      sync.Mutex
}

var upload *uploadData

func (d *uploadData) load() {
        d.mu.Lock()
        defer d.mu.Unlock()

        fi, err := ioutil.ReadDir("assets")
        if err != nil {
                fmt.Println(err)
                return
        }

        if len(fi) > 0 {
                d.Files = make([]string, len(fi))

                for i, f := range fi {
                        d.Files[i] = f.Name()
                }
        }
}

var tmpl = `
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>File Upload</title>
    <link type="text/css" rel="stylesheet" href="/assets/css/style.css" />
  </head>
  <body>
    <div class="container">
      <h1>File Upload</h1>
      <div class="message">{{.Message}}</div>
      <form class="form-signin" method="post" action="/upload" enctype="multipart/form-data">
          <fieldset>
            <input type="file" name="files" id="files">
            <input type="submit" name="submit" value="Submit">
        </fieldset>
      </form>
    </div>
    <div>
      <h3>Files</h3>
        {{range .Files}}
        <form class="form-files" method="post" action="/delete">
          <div>
            <input type="hidden" name="file" value={{.}}>
            <input type="submit" name="delete" value="Delete">
            <a href="assets/{{.}}">{{.}}</a>
          </div>
        </form>
        {{else}}
        <div>No files uploaded.</div>
        {{end}}
    </div>
 </body>
</html>
`
